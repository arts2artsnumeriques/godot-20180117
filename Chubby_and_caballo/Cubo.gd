extends TestCube

var vibrate = false
var sc
var sc_init

func _process(delta):
	if vibrate:
		sc += ( Vector3( 
			sc_init.x * randf() * 0.5,
			sc_init.y * randf() * 0.5,
			sc_init.z + randf() * (sc_init.z * 50 - sc_init.z)
		) - sc ) * delta * 2
	else:
		sc += (sc_init - sc) * delta * 0.3
	set_scale( sc )
	
func _input(event):
	if ( event.type == InputEvent.KEY ):
		if ( event.is_action_released( "GO_UP" ) ) :
			vibrate = false
		elif ( InputMap.event_is_action( event, "GO_UP" ) ):
			vibrate = true

func _ready():
	sc = get_scale()
	sc_init = get_scale()
	set_process( true )
	set_process_input( true )
