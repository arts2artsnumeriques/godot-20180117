extends SamplePlayer

var vibrate = false
var voiceID
var pitch

func _process(delta):
	if vibrate:
		pitch += ( ( 0.1 + randf() * 3 ) - pitch ) * delta * 0.2
		#print( get_pitch_scale( voiceID )  )
		#set_pan( voiceID, randf() )
	else:
		pitch += ( 1 - pitch ) * delta * 2
	set_pitch_scale( voiceID, pitch )

func _input(event):
	if ( event.type == InputEvent.KEY ):
		if ( event.is_action_released( "GO_UP" ) ) :
			vibrate = false
		elif ( InputMap.event_is_action( event, "GO_UP" ) ):
			vibrate = true

func _ready():
	set_process( true )
	set_process_input( true )
	voiceID = play("329077__vomitdog__wicked")
	pitch = get_pitch_scale( voiceID )
	pass
