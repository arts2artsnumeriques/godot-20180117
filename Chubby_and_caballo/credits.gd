extends Spatial

var mousepos = Vector2( 0,0 )
var vpsize = Vector2( 0,0 )

var crdori = Vector3(0,0,0)
var crdori_min = Vector3(-0.5,-0.05,-0.8)
var crdori_max = Vector3(-0.4,0.05,-0.3)
var crdtxt = null

func _fixed_process(delta):
	if vpsize != get_viewport().get_rect().size:
		vpsize =  get_viewport().get_rect().size
	var mapped = crdori_min + ( crdori_max - crdori_min ) * Vector3( mousepos.y,mousepos.y,mousepos.x )
	crdori += ( mapped - crdori ) * delta * 5
	crdtxt.set_rotation( crdori )

func _input(event):
	if ( event.type == InputEvent.MOUSE_MOTION ):
		mousepos = event.pos
		mousepos.x = ( mousepos.x / vpsize.width )
		mousepos.y = ( mousepos.y / vpsize.height )

func _ready():
	crdtxt = get_node( "credit-text" )
	set_fixed_process(true)
	set_process_input( true )
