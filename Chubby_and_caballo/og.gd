extends Spatial

export var file_prefix = ""
export(int, 'Datetime', 'Unix Timestamp') var file_tag
export(String, DIR) var output_path = "res://" setget set_output_path
var _tag = ""
var _index = 0

var dist = 20
var btn_displayed = true
var btn
var bg
var btn_bg_sc = Vector2( 1.0 / 64, 1.0 / 64 )
var color_min = Vector3( 0.75,0.6,0.36 )
var color_max = Vector3( 0.8,0.74,0.38 )
var color_angl = Vector3( 0,0,0 )
var color_speed = Vector3( 0.81, 0.96, 0.006 )
var main_color = Vector3(0,0,0)
var main_color_inverse = Vector3(1,1,1)
var alpha = 0
var alpha_mult = 1
var btn_text = "move, click, tab to sreenshot & esc to quit, ok?"
var btn_text_count = 1
var btn_text_wait = 1

var mousepos = Vector2( 0,0 )
var mousepos_prev = Vector2( 0,0 )
var models
var models_rot = Vector3(0,0,0)
var models_rot_target = Vector3(0,0,0)

var panels
var panel_rot_init
var panel_rot = 0

var cam
var cam_init
var cam_close = Vector3( 0,35,30 )
var cam_target

var model_sc_current
var model_sc_init
var model_sc_target
var model_sc_huge = Vector3( 10,10,10 )

var v0
var v1
var current_v
var do_swap = false
var sound_loop = 0
var roll_beat

var solar_repeat_count = 0
var solar_grid_count = 0

func _on_message_pressed():
	btn_displayed = false
	btn.hide()

func _process(delta):
	if btn_displayed or alpha_mult > 0:
		if btn_text_count <= btn_text.length():
			btn_text_wait += delta
			if btn_text_wait >= 0.01:
				btn_text_wait = 0
				btn.set_text( btn_text.substr( 0, btn_text_count ) )
				btn_text_count += 1
		else:
			btn_text_wait += delta
			if btn_text_wait >= 1.5:
				btn_text_wait = 0
				var txt = btn.get_text()
				btn_text_count += 1
				if btn_text_count == btn_text.length() + 5:
					btn.set_text( "click, sis-bro!" )
				elif btn_text_count == btn_text.length() + 12:
					_on_message_pressed()
				elif btn_text_count >= btn_text.length() + 10:
					btn.set_text( "ok, i'll do it for you, lazy gril-boy..." )
				else:
					btn.set_text( txt + "... ok?" )
		alpha += delta * 20
		color_angl += color_speed * delta
		var cmult = Vector3(
			( 1 + sin( color_angl.x * PI ) ) * 0.5,
			( 1 + sin( color_angl.y * PI ) ) * 0.5,
			( 1 + sin( color_angl.z * PI ) ) * 0.5 )
		main_color = color_min + ( color_max - color_min ) * cmult
		main_color_inverse = Vector3( 1,1,1 ) - main_color
		main_color_inverse = main_color_inverse.normalized()
		var vpsize = get_viewport().get_rect().size
		btn.set_size( Vector2(vpsize.width,vpsize.height) )
		bg.set_scale( Vector2(vpsize.width,vpsize.height) )
		var a = ( 0.95 + ( ( 1 + sin( alpha * PI ) ) * 0.3 ) * 0.1 ) * alpha_mult
		bg.set_modulate( Color( main_color.x, main_color.y, main_color.z, a ) )
	if !btn_displayed:
		alpha_mult -= delta
	
	if do_swap:
		current_v.hide()
		if current_v == v0:
			current_v = v1
		else:
			current_v = v0
		sound_loop += delta
		if sound_loop >= 0.08: 
			roll_beat = get_node("bip").play("369724__josepharaoh99__single-electro-beat")
			sound_loop = 0
		current_v.show()
	
	var mdelta = mousepos_prev - mousepos
	models_rot_target += Vector3( mdelta.y * PI * 0.8, mdelta.x * PI * 1.5, 0 )
	models_rot += ( models_rot_target - models_rot ) * delta * 1.2
	models.set_rotation( models_rot )
	mousepos_prev = mousepos
	
	panels.set_rotation( panel_rot_init + Vector3( 0, models_rot.y * 0.03, 0 ) )
	panel_rot += delta * 0.1
	var mult = 1
	for p in panels.get_children():
		p.set_rotation( Vector3( panel_rot * mult, 0, 0 ) )
		mult -= 0.1
	
	var cp = cam.get_translation()
	cp += ( cam_target - cp ) * delta * 5
	cam.set_translation( cp )
	cam.set_transform( cam.get_transform().looking_at( Vector3( 0,0,0 ), Vector3( 0,1,0 ) ) )
	
	model_sc_current += ( model_sc_target - model_sc_current ) * delta * 5
	models.set_scale( model_sc_current )


func _input(event):
	if ( event.type == InputEvent.MOUSE_MOTION ):
		mousepos_prev = mousepos
		mousepos = event.pos
		var vps = get_viewport().get_rect().size
		mousepos.x = ( ( mousepos.x / vps.width ) - 0.5 ) * 2
		mousepos.y = ( ( mousepos.y / vps.height ) - 0.5 ) * 2
	elif ( event.type == InputEvent.KEY ):
		if ( Input.is_key_pressed(KEY_ESCAPE) ):
			get_tree().change_scene("res://enter_the_wu.tscn")
		elif ( Input.is_key_pressed(KEY_TAB) ):
			make_screenshot()
	elif ( event.type == InputEvent.MOUSE_BUTTON ):
		if event.button_index == 1:
			if event.pressed:
				if cam_target != cam_close:
					get_node("slide").play("396705__chaosentertainment__dragslide1")
				cam_target = cam_close
				model_sc_target = model_sc_huge
			else:
				if cam_target != cam_init:
					get_node("slide").play("396705__chaosentertainment__dragslide1")
				cam_target = cam_init
				model_sc_target = model_sc_init
		elif event.button_index == 2:
			do_swap = event.pressed

func create_solars():
	var line = get_node("panels/line")
	var s0 = get_node("panels/line/solar0")
	var s1 = get_node("panels/line/solar1")
	var ns
	for si in range(0,solar_repeat_count):
		ns = s1.duplicate( true )
		line.add_child( ns )
		ns.set_translation( Vector3( si * 160, 0, 0 ) )
		ns.set_rotation( Vector3( -si * 0.2, 0, 0 ) )
	for liy in range(-solar_grid_count,solar_grid_count):
		for liz in range(-solar_grid_count,solar_grid_count):
			if liz == 0 and liy == 0:
				continue
			ns = line.duplicate( true )
			ns.set_translation( Vector3( 0, liy * 70, liz * 70 ) )
			panels.add_child( ns )

func _ready():
	
	btn = get_node("overlay/message")
	btn.set_text( "ctrls" )
	bg = get_node("overlay/bg")
	var vpsize = get_viewport().get_rect().size
	btn.set_pos( Vector2(0,0) )
	btn.set_size( Vector2(vpsize.width,vpsize.height) )
	bg.set_pos( Vector2(0,0) )
	bg.set_scale( Vector2(vpsize.width,vpsize.height) )
	
	_check_path(output_path)
	if not output_path[-1] == "/":
		output_path += "/"
	if not file_prefix.empty():
		file_prefix += "_"
	models = get_node( "models" )
	model_sc_current = models.get_scale()
	model_sc_init = models.get_scale()
	model_sc_target = models.get_scale()
	# creation of solars
	panels = get_node("panels")
	panel_rot_init = panels.get_rotation()
	panels.set_rotation( Vector3(0,0,0) )
	create_solars()
	panels.set_rotation( panel_rot_init )
	
	cam = get_node( "cam" )
	cam_init = cam.get_translation()
	cam_target = cam_init
	
	v0 = get_node("models/v0")
	v1 = get_node("models/v1")
	current_v = v0
	
	var s = get_node("ambient").get_sample_library().get_sample("330822__vendarro__old-big-engine-sound")
	s.set_loop_format( s.LOOP_FORWARD )
	s.set_loop_begin( 0 )
	s.set_loop_end( s.get_length() )
	get_node("ambient").play("330822__vendarro__old-big-engine-sound")
	
	#print( caballo, caballo_mat, chub.get_mesh(), chub_mat )
	#caballo_mati
	#chub_mati
	
	set_process( true )
	set_process_input( true )

############################
### screenshot functions ###
############################

func make_screenshot():
	get_viewport().queue_screen_capture()
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")		
	var image = get_viewport().get_screen_capture()
	_update_tags()
	image.save_png("%s%s%s_%s.png" % [output_path, file_prefix, _tag, _index])
	btn_displayed = true
	alpha_mult = 1
	btn_text_count = 0
	btn_text = "%s%s%s_%s.png" % [output_path, file_prefix, _tag, _index]
	btn_text += " saved! click"
	btn.set_text( "?" )
	btn.show()

func _check_path(path):
	if OS.is_debug_build():
		var message = 'WARNING: No directory "%s"'
		var dir = Directory.new()
		dir.open(path)
		if not dir.dir_exists(path):
			print(message % path)

func _update_tags():
	var time
	if (file_tag == 1): time = str(OS.get_unix_time())
	else:
		time = OS.get_datetime()
		time = "%s_%02d_%02d_%02d%02d%02d" % [time['year'], time['month'], time['day'], 
											time['hour'], time['minute'], time['second']]
	if (_tag == time): _index += 1
	else:
		_index = 0
	_tag = time	

func set_output_path(path):
	_check_path(path)
	output_path = path